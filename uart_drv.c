/*
 * uart_drv.c
 *
 *  Created on: Mar 27, 2017
 *      Author: gustavo
 */
#include "driverlib.h"
#include "uart_drv.h"
#include "rb.h"

static uart_drv_t uart_drv_list[UART_DRV_INSTANCES];
uint8_t uart_drv_rx_buffer_list[UART_DRV_INSTANCES][UART_DRV_BUFFER_MAX_LEN];
uint8_t uart_drv_tx_buffer_list[UART_DRV_INSTANCES][UART_DRV_BUFFER_MAX_LEN];


int8_t uart_drv_init(uint16_t base_address)
{

    static uint8_t idx = 0;
    int8_t ret = idx;

    if (idx < UART_DRV_INSTANCES)
    {
        rb_attr_t rx_rb_attr = { 1, UART_DRV_BUFFER_MAX_LEN, .buffer =
                                         uart_drv_rx_buffer_list[idx] };
        rb_attr_t tx_rb_attr = { 1, UART_DRV_BUFFER_MAX_LEN, .buffer =
                                         uart_drv_tx_buffer_list[idx] };

        rb_t rx_rb;
        rb_t tx_rb;

        rb_init(&rx_rb, &rx_rb_attr);
        rb_init(&tx_rb, &tx_rb_attr);

        uart_drv_list[idx].rx_rb = rx_rb;
        uart_drv_list[idx].tx_rb = tx_rb;
        uart_drv_list[idx].uart_drv_uart_base_address = base_address;

        uart_drv_uart_init(base_address);

        idx++;



    }
    else
    {
        ret = -1;
    }

    return ret;
}

int8_t uart_drv_tx_byte(uint8_t index, uint8_t byte)
{
    uint8_t tx_byte = byte;
    int8_t ret = 0;
    rb_t * tx_rb = &(uart_drv_list[index].tx_rb);

    ret = rb_put(tx_rb, (const void *) &tx_byte);
    uart_tx_fg = 1;
    return ret;
}

void uart_drv_tx_handler()
{
    //int8_t ret=0;
    int8_t i=0;
    rb_t * tx_buff;

    for (i=0; i<UART_DRV_INSTANCES; i++){
        tx_buff = &(uart_drv_list[i].tx_rb);

        if (!rb_empty(tx_buff)){
            P1OUT ^= BIT4;
            EUSCI_A_UART_enableInterrupt(uart_drv_list[i].uart_drv_uart_base_address,
                                         EUSCI_A_UART_TRANSMIT_INTERRUPT); // Enable interrupt
        }
    }
    //return ret;
}

void uart_drv_rx_handler()
{
    int8_t ret = 0;
    int8_t i=0;
    rb_t * rx_buff;
    uint8_t rx_data;

    for (i=0; i<UART_DRV_INSTANCES; i++){
        rx_buff = &(uart_drv_list[i].rx_rb);

        ret = rb_get(rx_buff, &rx_data);
        if (ret != -1){
            if (uart_drv_list[i].rx_cb != 0){
                uart_drv_list[i].rx_cb(rx_data);
            }
        }
    }
    //return ret;
}

static void uart_drv_uart_init(uint16_t base_address)
{
    // Configure UART
    EUSCI_A_UART_initParam param = { 0 };
    param.selectClockSource = EUSCI_A_UART_CLOCKSOURCE_ACLK;
    param.clockPrescalar = 3;
    param.firstModReg = 0;
    param.secondModReg = 92;
    param.parity = EUSCI_A_UART_NO_PARITY;
    param.msborLsbFirst = EUSCI_A_UART_LSB_FIRST;
    param.numberofStopBits = EUSCI_A_UART_ONE_STOP_BIT;
    param.uartMode = EUSCI_A_UART_MODE;
    param.overSampling = EUSCI_A_UART_LOW_FREQUENCY_BAUDRATE_GENERATION;

    if (STATUS_FAIL == EUSCI_A_UART_init(base_address, &param))
    {
        return;
    }

    EUSCI_A_UART_enable(base_address);

    EUSCI_A_UART_clearInterrupt(base_address, EUSCI_A_UART_RECEIVE_INTERRUPT);

    // Enable USCI_A0 RX interrupt
    EUSCI_A_UART_enableInterrupt(base_address, EUSCI_A_UART_RECEIVE_INTERRUPT); // Enable interrupt
}

#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A0_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A0_VECTOR)))
#endif
void EUSCI_A0_ISR(void)
{
    int8_t i = 0;
    uint8_t data = 0;

    switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
    {
    case USCI_NONE: break;
    case USCI_UART_UCRXIFG:
        data = EUSCI_A_UART_receiveData(EUSCI_A0_BASE);
        for (i=0; i<UART_DRV_INSTANCES; i++){
            if (uart_drv_list[i].uart_drv_uart_base_address == EUSCI_A0_BASE){
                rb_put(&(uart_drv_list[i].tx_rb), &data);
            }
        }
        __bic_SR_register_on_exit(LPM2_bits);
        break;
    case USCI_UART_UCTXIFG:

        for (i=0; i<UART_DRV_INSTANCES; i++){
            if (uart_drv_list[i].uart_drv_uart_base_address == EUSCI_A0_BASE){
                uint8_t data;
                if (rb_get(&(uart_drv_list[i].tx_rb),(void *) &data) != -1){

                    P1OUT ^= BIT0;
                    EUSCI_A_UART_transmitData(EUSCI_A0_BASE,
                                               data);
                }
                if (rb_empty(&(uart_drv_list[i].tx_rb))){
                    EUSCI_A_UART_disableInterrupt(EUSCI_A0_BASE,
                                                             EUSCI_A_UART_TRANSMIT_INTERRUPT); // Enable interrupt
                    uart_tx_fg = 0;
                }
            }
        }
        __bic_SR_register_on_exit(LPM1_bits);
        break;
    case USCI_UART_UCSTTIFG: break;
    case USCI_UART_UCTXCPTIFG: break;
    }
}
//
//#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
//#pragma vector=USCI_A1_VECTOR
//__interrupt
//#elif defined(__GNUC__)
//__attribute__((interrupt(USCI_A1_VECTOR)))
//#endif
//void EUSCI_A1_ISR(void)
//{
//    switch(__even_in_range(UCA1IV,USCI_UART_UCTXCPTIFG))
//    {
//    case USCI_NONE: break;
//    case USCI_UART_UCRXIFG:
//        RXData = EUSCI_A_UART_receiveData(EUSCI_A1_BASE);
//
//        break;
//    case USCI_UART_UCTXIFG: break;
//    case USCI_UART_UCSTTIFG: break;
//    case USCI_UART_UCTXCPTIFG: break;
//    }
//}


