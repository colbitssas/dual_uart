/*
 * th.h
 *
 *  Created on: Mar 28, 2017
 *      Author: gustavo
 *
 *  Task handler
 */

#ifndef TH_H_
#define TH_H_

#include <stdint.h>

#define TH_RET_SUCCESS          0
#define TH_RET_INV_PRIORITY     -1
#define TH_RET_NOAV_PRIORITY    -2

typedef void (* th_task_fcn_t)();

typedef struct {
    th_task_fcn_t fcn;
    uint8_t * flag;
} th_task_t;

int8_t th_task_register(th_task_fcn_t fcn, uint8_t * fg, uint8_t priority);
void th_task_handler();

#endif /* TH_H_ */
