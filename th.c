/*
 * th.c
 *
 *  Created on: Mar 28, 2017
 *      Author: gustavo
 */

#include "th.h"

#ifndef TH_MAX_PRIORITIES
#define TH_MAX_PRIORITIES   16
#endif


static th_task_t th_task_list[TH_MAX_PRIORITIES];

int8_t th_task_register(th_task_fcn_t fcn, uint8_t * fg, uint8_t priority){
    int8_t ret = 0;
    th_task_t * task = &(th_task_list[priority]);
    if (priority < TH_MAX_PRIORITIES){
        if (task->fcn == 0){
            task->fcn = fcn;
            task->flag = fg;
        } else {
            ret = TH_RET_NOAV_PRIORITY;
        }
    } else {
        ret = TH_RET_INV_PRIORITY;
    }

    return ret;
}

void th_task_handler(){
    int8_t i;
    th_task_t * curr_task;
    for (i=TH_MAX_PRIORITIES-1; i>=0; --i){
        if (*(th_task_list[i].flag) == 1){
            curr_task = &(th_task_list[i]);
            curr_task->fcn();
            // NOTE: This break allows a high priority task to execute
            // continuously during consecutive cycles.
            //break;
        }
    }

    // th_power_manage();
}
