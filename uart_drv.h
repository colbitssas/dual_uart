/*
 * uart_drv.h
 *
 *  Created on: Mar 27, 2017
 *      Author: gustavo
 */

#ifndef UART_DRV_H_
#define UART_DRV_H_

#include "rb.h"

#define UART_DRV_BUFFER_MAX_LEN 64

#define UART_DRV_INSTANCES 2

typedef void (* uart_drv_rx_cb_t)(uint8_t );

typedef struct {
    rb_t tx_rb;
    rb_t rx_rb;
    uint16_t uart_drv_uart_base_address;
    uart_drv_rx_cb_t rx_cb;

}uart_drv_t;

uint8_t uart_tx_fg;

int8_t uart_drv_init(uint16_t base_address);
static void uart_drv_uart_init(uint16_t base_address);
int8_t uart_drv_tx_byte(uint8_t index, uint8_t byte);
int8_t uart_drv_set_rx_cb(uint8_t instance, uart_drv_rx_cb_t rx_cb);
int8_t uart_drv_remove_rx_cb(uint8_t instance);
void uart_drv_tx_handler();
void uart_drv_rx_handler();

#endif /* UART_DRV_H_ */
