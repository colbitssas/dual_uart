///*
// * uart_interrupts.c
// *
// *  Created on: Mar 30, 2017
// *      Author: gustavo
// */
//
//
//
//#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
//#pragma vector=USCI_A0_VECTOR
//__interrupt
//#elif defined(__GNUC__)
//__attribute__((interrupt(USCI_A0_VECTOR)))
//#endif
//void EUSCI_A0_ISR(void)
//{
//    switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
//    {
//    case USCI_NONE: break;
//    case USCI_UART_UCRXIFG:
//        RXData = EUSCI_A_UART_receiveData(EUSCI_A0_BASE);
//        if(!(RXData == TXData))                 // Check value
//        {
//            while(1)
//            {
//                ;
//            }
//        }
//        break;
//    case USCI_UART_UCTXIFG: break;
//    case USCI_UART_UCSTTIFG: break;
//    case USCI_UART_UCTXCPTIFG: break;
//    }
//}
//
//#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
//#pragma vector=USCI_A1_VECTOR
//__interrupt
//#elif defined(__GNUC__)
//__attribute__((interrupt(USCI_A1_VECTOR)))
//#endif
//void EUSCI_A1_ISR(void)
//{
//    switch(__even_in_range(UCA1IV,USCI_UART_UCTXCPTIFG))
//    {
//    case USCI_NONE: break;
//    case USCI_UART_UCRXIFG:
//        RXData = EUSCI_A_UART_receiveData(EUSCI_A1_BASE);
//        if(!(RXData == TXData))                 // Check value
//        {
//            while(1)
//            {
//                ;
//            }
//        }
//        break;
//    case USCI_UART_UCTXIFG: break;
//    case USCI_UART_UCSTTIFG: break;
//    case USCI_UART_UCTXCPTIFG: break;
//    }
//}
//
