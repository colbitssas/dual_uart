//#include <msp430.h>
#include "uart_drv.h"
#include "driverlib.h"
#include "th.h"


const uint8_t * data[6] = {"HolaHolaHolaHolaHola\r\n",
                           "MundoMundoMundoMundoMundoMundo\r\n",
                           "Hola Mundo,Hola Mundo,Hola Mundo\r\n",
                           "Colbits, We are Making IOT greener and city wide\r\n",
                           "BLABLABLABLABLA\r\n",
                           "Tuururuuuru tuuurururu tuuurururu\r\n"};
const uint8_t data_len[6] = {22, 33, 34, 50, 17, 35};
int8_t uart1_instance = -1;
uint8_t rtc_rdy = 0;
uint8_t ps_fg = 0;
const uint8_t uart_fg = 1;


void rtc_init(){
    RTCCTL0_H = RTCKEY_H;                   // Unlock RTC

    RTCCTL0_L |= RTCRDYIE;  /* Enable Int */
    RTCCTL1 = RTCHOLD | RTCMODE;   // RTC enable, BCD mode, RTC hold

    //RTCPS1CTL = RT1IP_2 | RT1PSIE; /* Enable PS1, Interval /8 */
    RTCPS1CTL &= ~(RT1PSHOLD);

    RTCCTL1 &= ~(RTCHOLD);                  // Start RTC
}

void periodic_sender(){
    static uint8_t idx=0;
    static uint8_t msgs = 0;
    static uint8_t err_cnt = 0;
    uint8_t i=0;
    int8_t ret = 0;

    if (ps_fg){

        P1OUT ^= BIT3;

        for (i=0; i < data_len[idx]; i++){
            ret = uart_drv_tx_byte(uart1_instance, data[idx][i]);
            if (ret != 0){
                err_cnt++;
                break;
            }
        }

        idx++;
        msgs++;

        if (idx >= 6){
            idx = 0;
        }
        ps_fg = 0;
    }
}

/*
 * main.c
 */
int main(void) {
    // stop watchdog
    WDT_A_hold(WDT_A_BASE);

    // LFXT Setup
    //Set PJ.4 and PJ.5 as Primary Module Function Input.
    /*

     * Select Port J
     * Set Pin 4, 5 to input Primary Module Function, LFXT.
     */
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_PJ,
        GPIO_PIN4 + GPIO_PIN5,
        GPIO_PRIMARY_MODULE_FUNCTION
        );

    //Set DCO frequency to 8 MHz
    CS_setDCOFreq(CS_DCORSEL_1,CS_DCOFSEL_3);
    //Set external clock frequency to 32.768 KHz
    CS_setExternalClockSource(32768,0);
    //Set ACLK=XT1
    CS_initClockSignal(CS_ACLK,CS_XT1CLK_SELECT,CS_CLOCK_DIVIDER_1);
    //Start XT1 with no time out
    CS_turnOnXT1(CS_XT1_DRIVE_0);
    //Set SMCLK = DCO with frequency divider of 8
    CS_initClockSignal(CS_SMCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_8);
    //Set MCLK = DCO with frequency divider of 8
    CS_initClockSignal(CS_MCLK,CS_DCOCLK_SELECT,CS_CLOCK_DIVIDER_8);

    rtc_init();
    // Configure UART pins
    //Set P2.0 and P2.1 as Secondary Module Function Input.
    /*

     * Select Port 2d
     * Set Pin 0, 1 to input Secondary Module Function, (UCA0TXD/UCA0SIMO, UCA0RXD/UCA0SOMI).
     */
    GPIO_setAsPeripheralModuleFunctionInputPin(
        GPIO_PORT_P2,
        GPIO_PIN0 + GPIO_PIN1,
        GPIO_PRIMARY_MODULE_FUNCTION
        );

    P1DIR |= BIT0;
    P1OUT &= BIT0;

    P1DIR |= BIT4;
    P1OUT &= BIT4;

    P1DIR |= BIT3;
    P1OUT &= BIT3;


    PM5CTL0 &= ~(LOCKLPM5);

    uart1_instance = uart_drv_init(EUSCI_A0_BASE);

    th_task_register(periodic_sender, &ps_fg, 8);
    th_task_register(uart_drv_tx_handler, &uart_tx_fg, 10);
    th_task_register(uart_drv_rx_handler, &uart_fg, 12);

    __bis_SR_register(GIE);

    while(1){
        th_task_handler();
        __bis_SR_register(GIE | LPM1_bits);
    }
}

#pragma vector=RTC_VECTOR
__interrupt void RTC_ISR(void){
    switch(__even_in_range(RTCIV, RTCIV_RT1PSIFG)){
    case RTCIV_RTCRDYIFG:
        rtc_rdy=1;
        ps_fg=1;
        __bic_SR_register_on_exit(LPM1_bits);
        break;
    case RTCIV_RT1PSIFG:

        break;
    default:
        break;

    }
}
